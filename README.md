# NuttX device tree integration example

This is a project to test/demonstrate how the [dtgen](https://gitlab.com/nuttx_projects/dtgen) tool is used in NuttX to generate initialization code for devices.

# Test

Right now I'm adding dts support for stm32 family. You can test code generation by configuring for stm32f4discovery. To initialize this project, first to:

    make init

and then configure for the board with

    make configure BOARDCONFIG=stm324discovery:nsh

and finally build with

    make
